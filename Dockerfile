FROM openjdk:11.0.6-jdk
LABEL maintainer="sergio.silsop@gmail.com"
WORKDIR /workspace
RUN ls -la /workspace
COPY /target/silsop*.jar app.jar
RUN ls -la /workspace
EXPOSE 8912
ENTRYPOINT exec java -jar /workspace/app.jar