package com.devops.silsop;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.devops.silsop.custom.Dev;
import com.devops.silsop.custom.DevDAO;

@SpringBootTest
class SilsopApplicationTests {

	@Test
	void contextLoads() {
		
	}
	
	@Test
	public void add_and_get_lastadded_dev_test(){		   		
		Dev newMember = new Dev(4, "Alex", "Yalta", "anthder@hotmail.com");
		
		DevDAO dao = new DevDAO();	
		dao.addEmployee(newMember);
		
		System.out.println(newMember);
		System.out.println(dao.getAllEmployees().getDevList().get(3));;
		
	    assertThat(newMember).isEqualTo(dao.getAllEmployees().getDevList().get(3)); 
	}
	
	@Test
	public void low_level_developer_test() {
		Developer newbee = new Developer();
		newbee.setId(1);
		newbee.setName("Andres Garcia");
		newbee.setIq(100.1);
		assertThat(1).isEqualTo(newbee.getId());
		assertThat("Andres Garcia").isEqualTo(newbee.getName());
		assertThat(100.1).isEqualTo(newbee.getIq());
	}
	
	@Test
	public void low_level_dev_test() {
		Dev geek = new Dev();
		geek.setId(23);
		geek.setFirstName("Joseph");
		geek.setLastName("Green");
		geek.setEmail("joe_86@gmail.com");
		
		assertThat(23).isEqualTo(geek.getId());
		assertThat("Joseph").isEqualTo(geek.getFirstName());
		assertThat("Green").isEqualTo(geek.getLastName());
		assertThat("joe_86@gmail.com").isEqualTo(geek.getEmail());
	}
	

}
