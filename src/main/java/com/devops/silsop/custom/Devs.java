package com.devops.silsop.custom;

import java.util.ArrayList;
import java.util.List;

public class Devs {
    private List<Dev> devList;
    
    public List<Dev> getDevList() {
        if(devList == null) {
        	devList = new ArrayList<>();
        }
        return devList;
    }
  
    public void setDevList(List<Dev> employeeList) {
        this.devList = employeeList;
    }
}
