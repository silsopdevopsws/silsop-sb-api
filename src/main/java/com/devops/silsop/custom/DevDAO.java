package com.devops.silsop.custom;

import org.springframework.stereotype.Repository;

@Repository
public class DevDAO {
    private static Devs list = new Devs();
    
    static
    {
        list.getDevList().add(new Dev(1, "Sergio", "Silva", "sergio.silsop@gmail.com"));
        list.getDevList().add(new Dev(2, "Rosita", "Carranza", "rosmi1410@gmail.com"));
        list.getDevList().add(new Dev(3, "Saul", "Soplin", "lelhiel@gmail.com"));
    }
     
    public Devs getAllEmployees() 
    {
        return list;
    }
     
    public void addEmployee(Dev developer) {
        list.getDevList().add(developer);
    }
    
    
}
