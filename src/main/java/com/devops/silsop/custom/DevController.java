package com.devops.silsop.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DevController {
	   @Autowired
	   private DevDAO devDao;
	   
	   @RequestMapping(path="/devteam", produces = "application/json")
	    public Devs getEmployees() 
	    {
	        return devDao.getAllEmployees();
	    }
    
}
