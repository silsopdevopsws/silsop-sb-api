package com.devops.silsop.fun;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FunnyController {
	   @RequestMapping("/hookman")
	   public String getMessage() {
		   return "Hook man says : 'I know what you did last summer...'";
	   }
}
